"""
Define new functions using @qgsfunction. feature and parent must always be the
last args. Use args=-1 to pass a list of values as arguments
"""

from qgis.core import *
from qgis.gui import *
from qgis.utils import iface

@qgsfunction(args='auto', group='Custom')
def groenne_omr_areal(groenne_omraader_lyr, type_field, atlas_geom, feature, parent):
	
	if atlas_geom is None:
		raise Exception('Der mangler en Atlas geometri')
		
	lyr_groenne_omr =  QgsMapLayerRegistry.instance().mapLayersByName(groenne_omraader_lyr)[0]

	if lyr_groenne_omr is None:
		raise Exception("Layer not found: " + groenne_omraader_lyr)
		
	types = {}
	for feature in lyr_groenne_omr.getFeatures():
		lyr_groenne_omr_geom = feature.geometry()
		if lyr_groenne_omr_geom is None:
			continue
		if lyr_groenne_omr_geom.intersects(geom_atlas):
			if not feature[type_field] in types:
				types[feature[type_field]] = round(lyr_groenne_omr_geom.area(), 1)
			else:
				types[feature[type_field]] += round(lyr_groenne_omr_geom.area(), 1)
	
	html = ['<table style="border-collapse: collapse; font-size: 110%;" width=100%><tr><th>Type</th><th>Areal (kvm)</th></tr>']
	for type in types:
		html.append('<tr style="border: 1px solid #000">')
		html.append('<td style="border: 1px solid #000; padding-left: 5px;">{0}</td>'.format(type))
		html.append('<td style="border: 1px solid #000; padding-left: 5px;">{0}</td>'.format(types[type]))
		html.append('</tr>')
	html.append('</table>')
	return ''.join(html)
	
@qgsfunction(args='auto', group='Custom')
def groenne_omr_tid(feature, parent):
	pass


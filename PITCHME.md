## Grønne områder i Egedal
### Qgis

---

## Problem

Manglende overblik!

- Hvilke typer?|
- Hvor meget areal?|
- Tidsforbrug? |

Note:
Typer - hegn, græs, træer, buske mm. - Alt hvad der skal plejes
Tidsforbrug - Estimeret
Problemet er IKKE optimering af ruter!
+++

## Ønske fra "kunde"

PDF'er, som kan ses på iPad eller printes!

Note:
Kunde = Materialgården

+++

## Citat

> Vi har ikke brug for noget interaktivt fis! Det bøvler vi allerede med!

+++

![giveup](static/giveup.jpg)

Note:
Viser ret godt, hvordan jeg har det!

---

## Løsning

<p>QGIS + Atlas <span class="fragment"> + Python</span> <span class="fragment"> = 261 ens pdf'er med informationer for hvert arbejdsområde<p>

Note:
Vis eksempler på pdf'er

+++?image=static/stangkaer&size=auto 90%

+++?image=static/Toftehoejskolen&size=auto 90%

+++?image=static/Sandbjerg&size=auto 90%

---

## QGIS + Atlas

75% af arbejdet

Note:
Åbn QGIS projekt og vis indstillinger for Atlas

---

## Python

De sidste, men sjoveste 25%

Note:
Vis hvor function editor'en er

+++

## Pseudo-kode

**For hver feature:**

- Tjek om geometri overlapper arbejdsgeometri|
	- Nej --> Lav ingenting|
	- Ja  --> Find type og beregn areal|
		+ Findes typen allerede?|
			+ Nej --> Beregn areal og gem|
			+ Ja --> Læg areal til eksisterende areal|



+++?code=static/groenne_omraader.py&lang=python

@[10](Definer hvor funktionen placeres)
@[11](Definer en funktion, der tager tre variabler)
@[13-14](Tjek om der findes en Atlas geometri)
@[16](Find laget med grønne områder)
@[18-19](Tjek om laget findes)
@[21-30](Lav en dictionary med type som key og areal som value)
@[32-38](Lav en HTML tabel med resultater)
@[39](Returner HTML'en som en string)

Note:
Python dictionary = Json

---

<br>

@fa[twitter contact](@danielarnason85)

@fa[github contact](danielarnason)

@fa[linkedin](danielarnason)